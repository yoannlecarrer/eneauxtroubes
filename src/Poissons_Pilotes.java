
public class Poissons_Pilotes extends Thread  {
	
	private Zone[][] zone;
	private int coordoneeX;
	private int coordoneeY;
	private Requins requins;
	
	public Poissons_Pilotes(Zone[][] zone, int x, int y) {
        this.zone = zone;
        this.coordoneeX = x;
        this.coordoneeY = y;
    }
	
	public synchronized void attacherRequin() {
		zone[coordoneeX][coordoneeY].attendreRequins();
		setRequins(zone[coordoneeX][coordoneeY].getRequins());
    }
	
	public synchronized void lacherRequin() {
		requins.decroche();
		System.out.println(this+" a lacher");
		coordoneeX = requins.getX();
		coordoneeY = requins.getY();
		setRequins(null);
    }
	
	public Requins getRequins() {
		return requins;
	}

	public void setRequins(Requins requins) {
		this.requins = requins;
	}
	
	public void run() {
		for (int i = 3; i>0; i--) {
			attacherRequin();
			lacherRequin();
		}
	}
	

}
