
public class Zone {
	
	private int coordoneeX;
	private int coordoneeY;
	private int sardine;
	private boolean requinsZone = false;
	private Requins requins;
	
	
	public Zone(int x, int y, int sardine) {
        this.sardine = sardine;	
        this.coordoneeX = x;
        this.coordoneeY = y; 
    }
	
	public void manger() {
		if (sardine > 0 ){
			sardine--;
			System.out.println(Thread.currentThread().getName()+" Mange il reste "+sardine+" sardine sur le site "+coordoneeX+","+coordoneeY);
		}else {
			System.out.println(Thread.currentThread().getName()+" il n'y a plus de sardine dans la zone "+coordoneeX+","+coordoneeY);
		}
    }
	
	public synchronized void requinsEntreZone(){
		while(requinsZone == true){
			try {
				System.out.println(Thread.currentThread().getName()+" attend pour rentrer dans la zone : "+coordoneeX+","+coordoneeY);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(Thread.currentThread().getName()+" Entre dans la zone : "+coordoneeX+","+coordoneeY);
		setRequinsZone(true);
	}
	
	public synchronized void RequinsSorsZone(){
		setRequinsZone(false);
		System.out.println(Thread.currentThread().getName()+" Sors dans la zone : "+coordoneeX+","+coordoneeY);
		notifyAll();
	}
	
	public synchronized void avertirPoisonPilote() {
		notifyAll();
	}
	
	public synchronized void attendreRequins(){
		while(!requinsZone){
			try {
				//System.out.println(Thread.currentThread().getName()+" Attend un requins dans la zone : "+coordoneeX+","+coordoneeY);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		requins.accroche();
	}
	
	public int getSardine() {
		return sardine;
	}
	
	public int getY() {
		return coordoneeY;
	}
	
	public int getX() {
		return coordoneeX;
	}
	
	public boolean getNbRequins() {
		return requinsZone;
	}
	
	public synchronized void setRequinsZone(boolean requinsZone) {
		this.requinsZone = requinsZone;
	}

	public synchronized Requins getRequins() {
		return requins;
	}

	public synchronized void setRequins(Requins requins) {
		this.requins = requins;
	}
	
}
