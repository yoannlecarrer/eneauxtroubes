import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ocean {

	static final int MIN_SARDINE = 1;
	static final int MAX_SARDINE = 6;
	static final int MAX_REQUIN = 1;
	static final int MIN_REQUIN = 0;
	static final int MIN_POISSONSPILOTES = 0;
	static final int MAX_POISSONSPILOTES = 5;
	static final int TAILLE_OCEAN = 4;
	static final int NB_REQUINS_MAX = 3;
	static int nbTotalPoissonsPilotes = 0;
	static int nbTotalRequins = 0;
	Random rand = new Random();

	private Zone[][] zone = new Zone[TAILLE_OCEAN][TAILLE_OCEAN];
	private Poissons_Pilotes[] pilotes = new Poissons_Pilotes[100];
	private Requins[] requins = new Requins[NB_REQUINS_MAX];

	private List<Poissons_Pilotes> liste = new ArrayList<Poissons_Pilotes>();

	Ocean() {
		System.out.println("Initialsiaton de l'oc�ans");

		/* Instanciation des zone */
		for (int i = 0; i < TAILLE_OCEAN; i++) {
			for (int j = 0; j < TAILLE_OCEAN; j++) {

				int nbSardine = rand.nextInt(MAX_SARDINE - MIN_SARDINE + 1) + MIN_SARDINE;

				int nbPoissonsPilotes = rand.nextInt(MAX_POISSONSPILOTES - MIN_POISSONSPILOTES + 1)
						+ MIN_POISSONSPILOTES;

				if (nbTotalRequins != NB_REQUINS_MAX) {
					int nbRequins = rand.nextInt(MAX_REQUIN - MIN_REQUIN + 1) + MIN_REQUIN;

					/* Instanciation des Requins */
					for (int r = nbTotalRequins; r < nbTotalRequins + nbRequins; r++) {
						requins[r] = new Requins(zone, i, j);
						// System.out.println("r n�" + r);
					}

					nbTotalRequins += nbRequins;
				}

				// System.out.println( "nombre de Poisson Pilotes est de : " + nbPoissonsPilotes
				// + " dans la zone :" + i + "," + j);

				// System.out.println("nombre de requins est de : "+ nbRequins + " dans la zone
				// :" + i + "," + j);

				// System.out.println("nombre de sardines est de : " + nbSardine + " dans la
				// zone : " + i + "," + j);

				/* Instanciation des Poisson Pilotes */
				for (int p = nbTotalPoissonsPilotes; p < nbTotalPoissonsPilotes + nbPoissonsPilotes; p++) {
					pilotes[p] = new Poissons_Pilotes(zone, i, j);
					liste.add(pilotes[p]);
					// System.out.println("p n�" + p);
				}

				zone[i][j] = new Zone(i, j, nbSardine);

				nbTotalPoissonsPilotes += nbPoissonsPilotes;

			}
		}
	}

	public void lancerLaVie() {
		System.out.println("DEBUT");

		System.out.println("Nombre de Requins dans l'oc�an " + nbTotalRequins);

		System.out.println("Nombre de poisson Pilote dans l'oc�an " + nbTotalPoissonsPilotes);

		for (int i = 0; i < nbTotalRequins; i++) {
			requins[i].start();
		}

		for (int i = 0; i < nbTotalPoissonsPilotes; i++) {
			pilotes[i].start();
		}

		for (int i = 0; i < nbTotalPoissonsPilotes; i++) {
			try {
				pilotes[i].join();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (int i = 0; i < nbTotalRequins; i++) {
			try {
				requins[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("FINI");
	}

	public static void main(String[] args) {
		Ocean ocean = new Ocean();
		ocean.lancerLaVie();

	}

}
