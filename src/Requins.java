import java.util.Random;

public class Requins extends Thread {

	// Probl�me si deux requins on la m�me zone de d�part
	static final int NB_PLACE_MAX = 3;
	private int nbPlace = NB_PLACE_MAX;

	private Zone[][] zone;
	private int coordoneeX;
	private int coordoneeY;
	private boolean arrive = false;

	public Requins(Zone[][] zone, int x, int y) {
		this.zone = zone;
		this.coordoneeX = x;
		this.coordoneeY = y;
	}

	public synchronized Requins accroche() {
		while (nbPlace == 0) {
			try {
				System.out.println("Plus de place sur le requins"+this);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		nbPlace--;
		System.out.println(Thread.currentThread().getName()+" c'est accroch� il reste : " + nbPlace + " sur le requins : " +this);
		return this;
	}

	public synchronized void decroche() {
		while (arrive == false) {
			try {
				System.out.println(Thread.currentThread().getName()+" attend pour decrocher du requins");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		nbPlace++;
		System.out.println(Thread.currentThread().getName()+" a lacher il reste : " + nbPlace + " sur le requins : " +this);
		notifyAll();
	}

	public void mangerSardine() {
		zone[coordoneeX][coordoneeY].manger();
	}

	/*
	 * Le requin sors de la zone il set le requins de la zone a null
	 */
	public synchronized void sortirZone() {
		zone[coordoneeX][coordoneeY].RequinsSorsZone();
		zone[coordoneeX][coordoneeY].setRequins(null);
		notifyAll();
	}
	
	public synchronized void avertirPoisonPiloteRequins() {
		notifyAll();
	}

	public void entrerZone() {
		Random rand = new Random();
		int direction = rand.nextInt(3 - 0 + 1) + 0;

		if (direction == 0) {
			moveHaut();
		}

		else if (direction == 1) {
			moveGauche();
		}

		else if (direction == 2) {
			moveDroite();
		}

		else if (direction == 3) {
			moveBas();
		}
		zone[coordoneeX][coordoneeY].requinsEntreZone();
		zone[coordoneeX][coordoneeY].setRequins(this);
		arrive = true;
		this.avertirPoisonPiloteRequins();
		arrive = false;
		zone[coordoneeX][coordoneeY].avertirPoisonPilote();
		
		
	}

	public synchronized void moveHaut() {
		if (coordoneeX == 0) {
			coordoneeX = 3;
		} else {
			coordoneeX--;
		}
	}

	public synchronized void moveGauche() {
		if (coordoneeY == 0) {
			coordoneeY = 3;
		} else {
			coordoneeY--;
		}
	}

	public synchronized void moveDroite() {
		if (coordoneeY == 3) {
			coordoneeY = 0;
		} else {
			coordoneeY++;
		}
	}

	public synchronized void moveBas() {
		if (coordoneeX == 3) {
			coordoneeX = 0;
		} else {
			coordoneeX++;
		}
	}

	public int getY() {
		return coordoneeY;
	}

	public int getX() {
		return coordoneeX;
	}

	public void run() {
		for (int i = 5; i > 0; i--) {
			sortirZone();
			// dur�e du d�placement
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			entrerZone();
			// le temps de manger
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mangerSardine();
			// le temps de partir
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(this+" , "+ i);

		}
	}

}
